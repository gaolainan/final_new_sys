package com.explore.final_new_sys.mapper;

import com.explore.final_new_sys.pojo.News;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@Mapper
public interface NewsMapper extends BaseMapper<News> {

}
