package com.explore.final_new_sys.utils;

import com.explore.final_new_sys.mapper.ManagerMapper;
import com.explore.final_new_sys.pojo.Manager;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

public class GetAuthor {

    @Resource
    private ManagerMapper managerMapper;

    public String getName(Integer id){
        Manager manager = managerMapper.selectById(id);
        return manager.getUsername();
    }
}
