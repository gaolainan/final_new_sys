package com.explore.final_new_sys.utils;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.thymeleaf.Thymeleaf;

import java.util.ArrayList;

public class MybatisPlusGeneratorCode {

    public static void main(String[] args) {
        // 需要构建一个 代码自动生成器的对象
        AutoGenerator mpg = new AutoGenerator();
        // 配置策略

        // 1、全局配置
        GlobalConfig gc = new GlobalConfig();
        String propertyPath = System.getProperty("user.dir");
        gc.setOutputDir(propertyPath + "/src/main/java");
        gc.setAuthor("GaoLainan");
        gc.setOpen(false);
        gc.setFileOverride(false);// 是否覆盖
        gc.setServiceName("%Service"); // 去service的I前缀
        // gc.setIdType(IdType.ID_WORKER);

        mpg.setGlobalConfig(gc);

        // 2、设置数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/final_news_system?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 3、包的配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.explore.final_new_sys");
        pc.setEntity("pojo");
        pc.setController("controller");
        pc.setMapper("mapper");
        pc.setService("service");
        mpg.setPackageInfo(pc);

        // 4、策略配置置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude("attachment");
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true); //自动lombok
        strategy.setLogicDeleteFieldName("deleted");

        // 自动填充配置
        TableFill create = new TableFill("create_time", FieldFill.INSERT);
        TableFill update = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFill = new ArrayList<>();
        tableFill.add(create);
        tableFill.add(update);
        strategy.setTableFillList(tableFill);

        // 乐观锁配置
        strategy.setVersionFieldName("version");

        strategy.setRestControllerStyle(true);
        strategy.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(strategy);


        mpg.execute(); // 执行

    }
}