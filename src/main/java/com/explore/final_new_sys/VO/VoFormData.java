package com.explore.final_new_sys.VO;

import com.explore.final_new_sys.pojo.News;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class VoFormData {

    private String title;

    private String content;

    private Integer authorId;

    private MultipartFile file;
}
