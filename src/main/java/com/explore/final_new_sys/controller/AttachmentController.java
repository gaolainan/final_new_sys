package com.explore.final_new_sys.controller;


import com.explore.final_new_sys.VO.VoFormData;
import com.explore.final_new_sys.VO.VoNews;
import com.explore.final_new_sys.mapper.AttachmentMapper;
import com.explore.final_new_sys.mapper.NewsMapper;
import com.explore.final_new_sys.pojo.News;
import com.explore.final_new_sys.service.AttachmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@RestController
@RequestMapping("/attachment")
public class AttachmentController {

    @Autowired
    private AttachmentService attachmentService;

    @PostMapping("/upload")
    @ApiOperation(value = "上传文件照片")
    public boolean upload(VoFormData voFormData) {
        return attachmentService.insertFile(voFormData);
    }

    @PostMapping("/select")
    @ApiOperation(value = "选择对应的新闻")
    public VoNews selectById(Integer id){
        return attachmentService.selectByNewsId(id);
    }

}

