package com.explore.final_new_sys.controller;


import com.explore.final_new_sys.pojo.Manager;
import com.explore.final_new_sys.service.ManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@RestController
@RequestMapping("/manager")
public class ManagerController {
    @Autowired
    private ManagerService managerService;

    @ApiOperation(value = "用户用来登陆的接口")
    @PostMapping("/login")
    public Manager login(String account, String password){
        return managerService.login(account,password);
    }

    @PostMapping("/now")
    @ApiOperation(value = "获取当前登录用户的信息")
    public Manager query(){
        return managerService.queryUser();
    }

    @ApiOperation(value = "注册用户")
    @PostMapping("/register")
    public Boolean register(String account,String password){
        return managerService.register(account,password);
    }

    @PutMapping
    @ApiOperation(value = "更新用户的个人信息")
    public Boolean updateUserInfo(Manager user){
        return managerService.updateUserInfo(user);
    }

    @PostMapping("/query")
    @ApiOperation(value = "根据用户的id查询相关信息")
    public Manager selectUserById(Integer id){
        return managerService.selectUserById(id);
    }

    @GetMapping("/logout")
    @ApiOperation(value = "清除当前用户的登录状态")
    public Boolean logout(){
        return managerService.logUserOut();
    }

    @PostMapping("/queryAll")
    @ApiOperation(value = "管理员才有权限查询当前有多少用户")
    public List<Manager> queryAllManager(){
        return managerService.queryAll();
    }

    @PostMapping
    @ApiOperation(value = "根据用户名 查询模糊查找相关的用户信息")
    public List<Manager> searchByName(String name){
        return managerService.selectManagerByName(name);
    }

    @PostMapping("/del")
    @ApiOperation(value = "根据用户的id删除用户信息")
    public boolean deleteManagerById(Integer id){
        return managerService.deleteManagerById(id);
    }

    @PostMapping("/set")
    @ApiOperation(value = "把普通用户设置成管理员")
    public boolean setManger(Integer id){
        return managerService.setManger(id);
    }

}

