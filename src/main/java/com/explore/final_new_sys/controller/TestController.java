package com.explore.final_new_sys.controller;

import com.explore.final_new_sys.VO.VoFormData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.w3c.dom.ls.LSOutput;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RequestMapping("/up")
@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/upload")
    public String upload() {
        return "upload";
    }

    @PostMapping("/upload")
    @ResponseBody
    public String upload(VoFormData voFormData) {
        System.out.println("进来了"+voFormData.getAuthorId());
        if (voFormData.getFile().isEmpty()) {
            return "上传失败，请选择文件";
        }

        String fileName = voFormData.getFile().getOriginalFilename();
        System.out.println(fileName);
        String filePath = "C:\\Users\\le\\Desktop\\data\\";
        File dest = new File(filePath + fileName);
        try {
            voFormData.getFile().transferTo(dest);
            LOGGER.info("上传成功");
            return "上传成功";
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
        }
        return "上传失败！";
        //System.out.println(voFormData.toString());
        //return "hello";
    }


}

