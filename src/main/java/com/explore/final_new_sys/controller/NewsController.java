package com.explore.final_new_sys.controller;


import com.explore.final_new_sys.pojo.News;
import com.explore.final_new_sys.service.NewsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@RestController
@RequestMapping("/news")
public class NewsController {


    @Autowired
    private NewsService newsService;

    @ApiOperation(value = "增加新闻")
    @PostMapping("/add")
    public boolean insertNew(News news){
        return newsService.insertNews(news);
    }

    @ApiOperation(value = "根据新闻的id删除")
    @PostMapping("/del")
    public boolean deleteNew(Integer id){
        return newsService.deleteById(id);
    }

    @ApiOperation(value = "查询最新的五条新闻")
    @PostMapping("/top")
    public List<News> queryTop(){
        return newsService.selectTopNews();
    }

    @ApiOperation(value = "根据新闻的标题返回全部的结果")
    @PostMapping("/search")
    public List<News> searchByTitle(String title){
        return newsService.searchByTitle(title);
    }

    @ApiOperation(value = "查询当前登录的人所发布的新闻")
    @PostMapping
    public List<News> queryUserNew(){
        return newsService.queryUserNews();
    }

    @ApiOperation(value = "通过新闻的id查询新闻对应的相关信息")
    @PostMapping("/select")
    public News selectNewsById(Integer id){
        return newsService.selectNewsById(id);
    }

    @PutMapping("/update")
    @ApiOperation(value = "更新新闻的信息")
    public boolean updateNewsInfo(News news){
        return newsService.updateNewInfo(news);
    }


    @PostMapping("/queryAll")
    @ApiOperation(value = "查询全部的新闻列表")
    public List<News> queryAllNews(){
        return newsService.queryAll();
    }

    @PostMapping("/selectNo")
    @ApiOperation(value = "查找未审核的新闻")
    public List<News> queryNo(){
        return newsService.queryNoPublish();
    }

    @PostMapping("/judge")
    @ApiOperation(value = "根据新闻的id，审核通过待审核的新闻")
    public boolean judgeNew(Integer id){
        return newsService.judgeNews(id);
    }
}

