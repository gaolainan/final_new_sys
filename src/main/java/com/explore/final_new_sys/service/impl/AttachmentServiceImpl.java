package com.explore.final_new_sys.service.impl;

import com.explore.final_new_sys.VO.VoFormData;
import com.explore.final_new_sys.VO.VoNews;
import com.explore.final_new_sys.controller.AttachmentController;
import com.explore.final_new_sys.mapper.ManagerMapper;
import com.explore.final_new_sys.mapper.NewsMapper;
import com.explore.final_new_sys.pojo.Attachment;
import com.explore.final_new_sys.mapper.AttachmentMapper;
import com.explore.final_new_sys.pojo.Manager;
import com.explore.final_new_sys.pojo.News;
import com.explore.final_new_sys.service.AttachmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.explore.final_new_sys.utils.GetAuthor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@Service
@Slf4j
public class AttachmentServiceImpl extends ServiceImpl<AttachmentMapper, Attachment> implements AttachmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentController.class);

    @Resource
    private AttachmentMapper attachmentMapper;

    @Resource
    private NewsMapper newsMapper;

    @Resource
    private ManagerMapper managerMapper;

    @Override
    public boolean insertFile(VoFormData voFormData) {
        Attachment attachment = new Attachment();
        News news = new News();
        news.setTitle(voFormData.getTitle());
        news.setContent(voFormData.getContent());
        news.setAuthorId(voFormData.getAuthorId());
        int insert = newsMapper.insert(news);
        if (insert == 0)
            return false;
        attachment.setNewId(news.getId());
        String fileName = voFormData.getFile().getOriginalFilename();
        String filePath = "C:\\Users\\le\\Desktop\\data\\";
        File dest = new File(filePath + fileName);
        try {
            voFormData.getFile().transferTo(dest);
            LOGGER.info("上传成功");
            attachment.setFilename(filePath+fileName);
            int i = attachmentMapper.insert(attachment);
            if (i != 0)
                return true;
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
        }
        return false;
    }

    @Override
    public VoNews selectByNewsId(Integer id) {
        News news = newsMapper.selectById(id);
        VoNews voNews = new VoNews();
        Manager manager = managerMapper.selectById(news.getAuthorId());
        voNews.setAuthor(manager.getUsername());
        voNews.setContent(news.getContent());
        voNews.setCreateTime(news.getCreateTime());
        voNews.setId(news.getId());
        voNews.setPublishTime(news.getPublishTime());
        voNews.setTitle(news.getTitle());
        HashMap<String, Object> map = new HashMap<>();
        map.put("new_id",id);
        List<Attachment> attachments = attachmentMapper.selectByMap(map);
        if(attachments.size() == 0){
            voNews.setPath("");
            return voNews;
        }
        voNews.setPath(attachments.get(0).getFilename());
        return voNews;
    }
}
