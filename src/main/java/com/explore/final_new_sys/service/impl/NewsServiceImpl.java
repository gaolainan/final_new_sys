package com.explore.final_new_sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.explore.final_new_sys.mapper.ManagerMapper;
import com.explore.final_new_sys.pojo.Manager;
import com.explore.final_new_sys.pojo.News;
import com.explore.final_new_sys.mapper.NewsMapper;
import com.explore.final_new_sys.service.NewsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.explore.final_new_sys.utils.ServletUtils;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements NewsService {

    @Resource
    private NewsMapper newsMapper;

    @Resource
    private ManagerMapper managerMapper;

    @Override
    public List<News> selectTopNews() {
        QueryWrapper<News> wrapper = new QueryWrapper<>();
        wrapper
                .eq("status",1)
                .orderByDesc("create_time");
        List<News> newsList = newsMapper.selectList(wrapper);
        List<News> res = new ArrayList<>();
        //newsList.forEach(System.out::print);
        // i的大小表示返回的新闻list的大小

        for (int i = 0; i < 8; i++) {
            //System.out.println(newsList.get(i));
            res.add(newsList.get(i));
        }
        return res;
    }

    // 插入成功返回true 反之返回false
    // 用户必须登录 才能发布新闻
    @Override
    public boolean insertNews(News news) {
        //Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        //if (userId == null)
            //return false;
        //news.setAuthorId(userId);
        return newsMapper.insert(news) != 0?true:false;
    }

    // 根据id删除新闻的数据 如果删除成功 则返回1 失败为0
    @Override
    public boolean deleteById(Integer id) {
        return newsMapper.deleteById(id)!=0?true:false;
    }

    @Override
    public List<News> searchByTitle(String title) {
        QueryWrapper<News> wrapper = new QueryWrapper<>();
        wrapper
                .like("title",title);
        List<News> list = newsMapper.selectList(wrapper);
        return list;
    }

    @Override
    public List<News> queryUserNews() {
        Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        HashMap<String, Object> map = new HashMap<>();
        map.put("author_id",userId);
        List<News> list = newsMapper.selectByMap(map);
        list.forEach(System.out::println);
        return list;
    }

    @Override
    public News selectNewsById(Integer id) {
        News news = newsMapper.selectById(id);
        if (news == null)
            return null;
        return news;
    }

    @Override
    public boolean updateNewInfo(News news) {
        Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        Manager manager = managerMapper.selectById(userId);
        if (manager.getRoleId() != 1)
            return false;
        int i = newsMapper.updateById(news);
        System.out.println(i);
        return i==0 ? false:true;
    }

    @Override
    public List<News> queryAll() {

        QueryWrapper<News> wrapper = new QueryWrapper<>();
        wrapper
                .isNotNull("title");
        List<News> news = newsMapper.selectList(wrapper);
        if (news.size() == 0)
            return null;
        return news;
    }

    @Override
    public List<News> queryNoPublish() {
        QueryWrapper<News> wrapper = new QueryWrapper<>();
        wrapper
                .eq("status",0);
        List<News> list = newsMapper.selectList(wrapper);
        if (list.size() != 0)
            return list;
        return null;
    }

    @Override
    public boolean judgeNews(Integer id) {
        News news = newsMapper.selectById(id);
        if (news == null)
            return false;
        news.setPublishTime(LocalDateTime.now());
        UpdateWrapper<News> wrapper = new UpdateWrapper<>();
        wrapper
                .eq("id",id)
                .set("status",1);
        //nt update = newsMapper.updateById(news);
        return newsMapper.update(news,wrapper) == 0?false:true;
    }
}
