package com.explore.final_new_sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.explore.final_new_sys.pojo.Manager;
import com.explore.final_new_sys.mapper.ManagerMapper;
import com.explore.final_new_sys.pojo.News;
import com.explore.final_new_sys.service.ManagerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.explore.final_new_sys.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, Manager> implements ManagerService {

    @Resource
    private ManagerMapper managerMapper;

    @Override
    public Manager login(String account, String password) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("username",account);
        List<Manager> list = managerMapper.selectByMap(map);
        if (list.size() == 0)
            return null;
        Manager manager = list.get(0);
        if (!manager.getPassword().equals(password))
            return null;
        /*HttpSession session = ServletUtils.getSession();
        session.setAttribute("userId",manager.getId());
        session.setAttribute("userName",manager.getUsername());*/
        return manager;
    }

    @Override
    public Manager queryUser() {
        Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        Manager manager = managerMapper.selectById(userId);
        System.out.println(manager);
        return manager;
    }

    @Override
    public Boolean register(String account,String password) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("username",account);
        List<Manager> list = managerMapper.selectByMap(map);
        if (list.size() != 0)
            return false;
        Manager user = new Manager();
        user.setUsername(account);
        user.setPassword(password);
        System.out.println(managerMapper.insert(user));
        return true;
    }


    @Override
    public Boolean updateUserInfo(Manager manager) {
        Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        manager.setId(userId);
        int i = managerMapper.updateById(manager);
        System.out.println(i);
        return i==0 ? false:true;
    }

    /**
     * 根据用户的id的查询用户的所有信息
     * @param id
     * @return
     */
    @Override
    public Manager selectUserById(Integer id) {
        Manager manager = managerMapper.selectById(id);
        return manager != null ? manager:null;
    }

    /**
     * 清除当前的session
     * @return
     */
    @Override
    public Boolean logUserOut() {
        return ServletUtils.clearSession()?true:false;
    }

    @Override
    public List<Manager> queryAll() {
        /*Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        Manager manager = managerMapper.selectById(userId);
        if (manager.getRole() != 1)
            return null;*/
        QueryWrapper<Manager> wrapper = new QueryWrapper<>();
        wrapper
                .isNotNull("username");
        List<Manager> list = managerMapper.selectList(wrapper);
        if (list.size() == 0)
            return null;
        return list;
    }

    @Override
    public List<Manager> selectManagerByName(String name) {
        /*Integer userId = Integer.valueOf(ServletUtils.getSession().getAttribute("userId").toString());
        Manager manager = managerMapper.selectById(userId);
        if (manager.getRole() != 1)
            return null;*/
        QueryWrapper<Manager> wrapper = new QueryWrapper<>();
        wrapper
                .like("username",name);
        List<Manager> list = managerMapper.selectList(wrapper);
        if(list.size() == 0)
            return null;
        return list;
    }

    @Override
    public boolean deleteManagerById(Integer id) {
        return managerMapper.deleteById(id) != 0?true:false;
    }

    @Override
    public boolean setManger(Integer id) {
        Manager manager = managerMapper.selectById(id);
        if (manager == null)
            return false;
        manager.setUpdateTime(LocalDateTime.now());
        UpdateWrapper<Manager> wrapper = new UpdateWrapper<>();
        wrapper
                .eq("id",id)
                .set("role_id",1);
        //nt update = newsMapper.updateById(news);
        return managerMapper.update(manager,wrapper) == 0?false:true;
    }
}
