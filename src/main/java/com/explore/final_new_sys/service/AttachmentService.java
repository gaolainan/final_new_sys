package com.explore.final_new_sys.service;

import com.explore.final_new_sys.VO.VoFormData;
import com.explore.final_new_sys.VO.VoNews;
import com.explore.final_new_sys.pojo.Attachment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.explore.final_new_sys.pojo.News;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
public interface AttachmentService extends IService<Attachment> {

    /**
     * 上传文件到指定的文件夹中
     * @param voFormData
     * @return
     */
    public boolean insertFile(VoFormData voFormData);

    public VoNews selectByNewsId(Integer id);
}
