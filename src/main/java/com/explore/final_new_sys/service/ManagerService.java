package com.explore.final_new_sys.service;

import com.explore.final_new_sys.pojo.Manager;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
public interface ManagerService extends IService<Manager> {

    /**
     * 用来验证用户登录的步骤
     * @param account
     * @param password
     * @return
     */
    public Manager login(String account,String password);

    /**
     * 查询用户的相关信息
     * @return
     */
    public Manager queryUser();

    /**
     * 注册用户的信息
     * @param account
     * @param password
     * @return
     */
    public Boolean register(String account,String password);

    /**
     * 更新用户自己的信息
     * @param user
     * @return
     */
    public Boolean updateUserInfo(Manager manager);

    /**
     * 根据用户的id查询用户的相关信息
     * @param id
     * @return
     */
    public Manager selectUserById(Integer id);

    /**
     * 清楚当前用户的session
     * @return
     */
    public Boolean logUserOut();

    /**
     * 管理员有权限查看所有的用户状态
     * @return
     */
    public List<Manager> queryAll();

    /**
     * 根据用户名查询相关的
     * @param name
     * @return
     */
    public List<Manager> selectManagerByName(String name);

    /**
     * 根据用户的id删除用户的信息
     * @param id
     * @return
     */
    public boolean deleteManagerById(Integer id);

    /**
     * 根据id设置管理员
     * @param id
     * @return
     */
    public boolean setManger(Integer id);
}
