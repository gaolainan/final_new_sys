package com.explore.final_new_sys.service;

import com.explore.final_new_sys.pojo.News;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoLainan
 * @since 2020-10-29
 */
public interface NewsService extends IService<News> {

    /**
     * 查询当前的发布的时间
     * 最新的五条新闻
     * @return
     */
    List<News> selectTopNews();

    /**
     * 向数据库中插入发布的新闻
     * @param news
     * @return
     */
    boolean insertNews(News news);

    /**
     * 根据新闻的标题删除
     * @param id
     * @return
     */
    boolean deleteById(Integer id);

    /**
     * 跟据新闻的标题模糊查找
     * @param title
     * @return
     */
    List<News> searchByTitle(String title);

    /**
     * 查询当前登录得用户的发布的新闻
     * @return
     */
    List<News> queryUserNews();

    /**
     * 根据新闻的id查询对应的id
     * @param id
     * @return
     */
    News selectNewsById(Integer id);

    /**
     * 更新新闻的大体信息
     * @param news
     * @return
     */
    public boolean updateNewInfo(News news);

    /**
     * 查询全部的新闻返回list集合
     * @return
     */
    public List<News> queryAll();

    /**
     * 返回未审核饿新闻
     * @return
     */
    public List<News> queryNoPublish();

    /**
     * 通过新闻的审核
     * @param id
     * @return
     */
    public boolean judgeNews(Integer id);
}
