package com.explore.final_new_sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan(basePackages = "com.explore.final_new_sys.mapper")
public class FinalNewSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalNewSysApplication.class, args);
    }

}
