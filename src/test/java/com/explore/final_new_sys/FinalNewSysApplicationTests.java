package com.explore.final_new_sys;

import com.explore.final_new_sys.VO.VoNews;
import com.explore.final_new_sys.mapper.AttachmentMapper;
import com.explore.final_new_sys.service.AttachmentService;
import com.explore.final_new_sys.service.ManagerService;
import com.explore.final_new_sys.service.NewsService;
import com.explore.final_new_sys.utils.GetAuthor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;

@SpringBootTest
class FinalNewSysApplicationTests {

    @Autowired
    private NewsService newsService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private ManagerService managerService;

    @Resource
    AttachmentMapper attachmentMapper;

    @Test
    void contextLoads() {
        //VoNews voNews = attachmentService.selectByNewsId(13);
        //System.out.println(voNews);
        //HashMap<String, Object> map = new HashMap<>();
        //map.put("new_id",1);
        //attachmentMapper.selectByMap(map).forEach(System.out::print);\\
        //boolean b = managerService.setManger(4);
        //System.out.println(attachmentService.selectByNewsId(3));
        boolean b = managerService.setManger(7);
        System.out.println(b);
    }

}
