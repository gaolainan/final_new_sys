/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80020
Source Host           : localhost:3306
Source Database       : final_news_system

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2020-11-11 12:35:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for attachment
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `new_id` int DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `deleted` int DEFAULT '0',
  `version` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `newsId` (`new_id`),
  CONSTRAINT `newsId` FOREIGN KEY (`new_id`) REFERENCES `news` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attachment
-- ----------------------------
INSERT INTO `attachment` VALUES ('1', '1', 'C:\\Users\\le\\Desktop\\data\\5.png', '', '2020-11-08 07:11:44', '0', '1');
INSERT INTO `attachment` VALUES ('2', '3', 'C:\\Users\\le\\Desktop\\data\\9.png', null, '2020-11-08 07:18:39', '0', '1');
INSERT INTO `attachment` VALUES ('3', '20', 'C:\\Users\\le\\Desktop\\data\\6.png', null, '2020-11-11 03:54:21', '0', '1');
INSERT INTO `attachment` VALUES ('4', '21', 'C:\\Users\\le\\Desktop\\data\\5.png', null, '2020-11-11 03:59:28', '0', '1');
INSERT INTO `attachment` VALUES ('5', '22', 'C:\\Users\\le\\Desktop\\data\\3.png', null, '2020-11-11 04:00:40', '0', '1');
INSERT INTO `attachment` VALUES ('6', '23', 'C:\\Users\\le\\Desktop\\data\\2.png', null, '2020-11-11 04:02:51', '0', '1');
INSERT INTO `attachment` VALUES ('7', '24', 'C:\\Users\\le\\Desktop\\data\\4.png', null, '2020-11-11 04:03:25', '0', '1');
INSERT INTO `attachment` VALUES ('8', '25', 'C:\\Users\\le\\Desktop\\data\\5.png', null, '2020-11-11 04:04:21', '0', '1');
INSERT INTO `attachment` VALUES ('9', '27', 'C:\\Users\\le\\Desktop\\data\\4.png', null, '2020-11-11 04:06:18', '0', '1');

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_pass` int DEFAULT NULL,
  `deleted` int DEFAULT '0',
  `version` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', 'admin', '123456', '1', '2020-10-29 10:23:38', null, '1', '0', '1');
INSERT INTO `manager` VALUES ('2', 'uhu', '123456', '0', '2020-10-29 13:09:40', null, null, '1', '1');
INSERT INTO `manager` VALUES ('3', 'user01', '123', '1', '2020-10-29 05:14:03', '2020-11-10 13:41:57', null, '0', '1');
INSERT INTO `manager` VALUES ('4', 'user02', '123', '1', '2020-10-28 13:14:54', '2020-11-09 02:20:48', null, '0', '1');
INSERT INTO `manager` VALUES ('5', 'user03', '123', '0', '2020-10-29 13:16:24', null, null, '0', '1');
INSERT INTO `manager` VALUES ('6', 'user04', '123', '0', '2020-10-29 13:17:06', null, null, '0', '1');
INSERT INTO `manager` VALUES ('7', 'user05', '123', '0', '2020-10-29 05:17:58', '2020-11-10 06:53:52', null, '0', '1');
INSERT INTO `manager` VALUES ('8', 'user06', '123', '1', '2020-10-29 05:18:27', '2020-11-10 13:50:45', null, '0', '1');
INSERT INTO `manager` VALUES ('9', 'user07', '123', '1', '2020-10-29 05:23:14', '2020-11-10 13:50:39', null, '0', '1');
INSERT INTO `manager` VALUES ('10', 'user09', '231', '1', '2020-10-29 13:25:48', null, null, '0', '1');
INSERT INTO `manager` VALUES ('11', 'user08', '123', '0', '2020-10-29 13:27:19', null, null, '0', '1');
INSERT INTO `manager` VALUES ('12', 'user10', '123', '0', '2020-10-29 13:27:54', null, null, '0', '1');
INSERT INTO `manager` VALUES ('13', 'aeda', 'da', '0', '2020-10-29 13:33:31', null, null, '1', '1');
INSERT INTO `manager` VALUES ('14', 'user11', '123', '0', '2020-10-30 06:02:43', null, null, '0', '1');
INSERT INTO `manager` VALUES ('15', 'adadad', '123', '0', '2020-10-30 06:29:09', null, null, '0', '1');
INSERT INTO `manager` VALUES ('16', 'ad', '123', '1', '2020-10-30 06:30:07', null, null, '0', '1');
INSERT INTO `manager` VALUES ('17', 'Xxxxaxa', '123', '0', '2020-10-30 06:38:06', null, null, '1', '1');
INSERT INTO `manager` VALUES ('18', 'dawda', '123', '0', '2020-10-30 06:46:16', null, null, '1', '1');
INSERT INTO `manager` VALUES ('19', 'admin01', '123456', '0', '2020-10-30 07:03:05', null, null, '1', '1');
INSERT INTO `manager` VALUES ('20', 'admin02', '123qwe', '0', '2020-10-30 07:51:49', null, null, '0', '1');
INSERT INTO `manager` VALUES ('21', 'user20', '123', '1', '2020-11-06 03:46:47', '2020-11-11 04:12:47', null, '0', '1');
INSERT INTO `manager` VALUES ('22', 'administarator', '123', '0', '2020-11-10 09:11:39', null, null, '0', '1');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int DEFAULT '0',
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `publish_time` datetime DEFAULT NULL,
  `deleted` int DEFAULT '0',
  `version` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `userId` (`author_id`),
  CONSTRAINT `userId` FOREIGN KEY (`author_id`) REFERENCES `manager` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '1', '要求', '1', '啊达瓦达瓦啊', '2020-10-28 06:51:52', '2020-11-09 02:40:56', '0', '1');
INSERT INTO `news` VALUES ('2', '1', '要和法塔赫发帖', '1', '1d大大伟大伟大', '2020-10-28 14:51:52', '2020-11-09 02:41:01', '0', '1');
INSERT INTO `news` VALUES ('3', '1', '要否合法', '1', '阿达达瓦达瓦', '2020-10-28 22:51:52', '2020-11-08 10:42:00', '0', '1');
INSERT INTO `news` VALUES ('4', '1', '要否合法', '1', '1和返回发帖和福特回复', '2020-10-28 22:51:52', '2020-11-09 02:41:27', '0', '1');
INSERT INTO `news` VALUES ('5', '1', '要求', '1', '1规划土方回填', '2020-10-28 22:51:52', '2020-11-09 02:41:30', '0', '1');
INSERT INTO `news` VALUES ('6', '1', '要求回复恢复通航', '1', '1还富含糖分和法塔赫发帖回复', '2020-10-28 22:51:52', '2020-11-09 02:41:32', '0', '1');
INSERT INTO `news` VALUES ('7', '1', '要 否合法', '1', '1h\'t\'f\'hh\'f', '2020-10-28 22:51:52', '2020-11-09 02:41:35', '1', '1');
INSERT INTO `news` VALUES ('8', '1', '要化肥挥发发', '1', '会复活复活', '2020-10-28 22:51:52', '2020-11-09 02:41:37', '0', '1');
INSERT INTO `news` VALUES ('9', '1', '复活复活', '1', '1h\'f\'f', '2020-10-28 22:51:52', '2020-11-09 02:41:39', '1', '1');
INSERT INTO `news` VALUES ('10', '1', '要求h\'f\'h', '1', '1否合法', '2020-10-28 22:51:52', '2020-11-09 02:41:46', '0', '1');
INSERT INTO `news` VALUES ('11', '1', '化肥挥', '1', '会耗费', '2020-10-28 22:51:52', '2020-11-09 02:41:59', '1', '1');
INSERT INTO `news` VALUES ('12', '1', '要好烦好烦', '1', '1f\'h\'f\'h\'ffh\'f\'h发', '2020-10-28 22:51:52', '2020-11-09 02:42:02', '0', '1');
INSERT INTO `news` VALUES ('13', '1', '要好烦好烦', '1', '返回发帖和福特军事法庭也就是在增加', '2020-10-28 22:51:52', '2020-11-09 02:42:05', '0', '1');
INSERT INTO `news` VALUES ('14', '1', '就这', '1', '<p>dadwadwadwaf发归属感傻瓜是公司公司&nbsp;</p>', '2020-11-09 06:33:10', '2020-11-10 07:08:20', '0', '1');
INSERT INTO `news` VALUES ('15', '4', '大大', '1', 'dadawd我是衣阿华被你的把框架卡号卡密 大会的跨行点卡大的卡号打里发哈伦开发哈', '2020-11-09 07:21:33', '2020-11-10 07:22:41', '1', '1');
INSERT INTO `news` VALUES ('16', '4', '大得发疯哇', '1', 'dadawd我是衣阿华被你', '2020-11-09 23:26:27', '2020-11-10 07:26:49', '0', '1');
INSERT INTO `news` VALUES ('17', '4', '打大大', '1', 'dadawd我是衣阿华被你', '2020-11-09 23:29:05', '2020-11-10 07:29:16', '0', '1');
INSERT INTO `news` VALUES ('18', '5', 'dad', '1', 'dadwadwadwa', '2020-11-10 01:14:58', '2020-11-10 09:22:51', '0', '1');
INSERT INTO `news` VALUES ('19', '5', 'dad', '0', 'dadwadwadwa', '2020-11-10 09:15:00', null, '0', '1');
INSERT INTO `news` VALUES ('20', '1', null, '0', '                            <!-- <p class=\"myEdit\">请输入文本的内容</p> -->\r\n                        ', '2020-11-11 03:54:21', null, '0', '1');
INSERT INTO `news` VALUES ('21', '1', null, '0', '                            <!-- <p class=\"myEdit\">请输入文本的内容</p> -->\r\n                        ', '2020-11-11 03:59:27', null, '0', '1');
INSERT INTO `news` VALUES ('22', '1', '大大', '0', '                            <!-- <p class=\"myEdit\">请输入文本的内容</p> -->\r\n                        ', '2020-11-11 04:00:40', null, '1', '1');
INSERT INTO `news` VALUES ('23', '1', '第一天学习这个项目', '0', '                            <!-- <p class=\"myEdit\">请输入文本的内容</p> -->\r\n                        ', '2020-11-11 04:02:51', null, '1', '1');
INSERT INTO `news` VALUES ('24', '1', '第一天学习这个项目', '0', '\r\n                        ', '2020-11-11 04:03:24', null, '1', '1');
INSERT INTO `news` VALUES ('25', '1', '第一天学习这个项目', '0', '', '2020-11-11 04:04:21', null, '1', '1');
INSERT INTO `news` VALUES ('26', '1', '', '0', '', '2020-11-11 04:06:03', null, '1', '1');
INSERT INTO `news` VALUES ('27', '1', 'sqD', '0', '<p>f\'fAFwafwa</p>', '2020-11-11 04:06:18', null, '0', '1');
INSERT INTO `news` VALUES ('28', '1', 'DAWDWA', '0', '', '2020-11-11 04:06:40', null, '1', '1');
